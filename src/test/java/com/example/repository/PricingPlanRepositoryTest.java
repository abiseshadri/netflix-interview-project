package com.example.repository;

import static org.junit.Assert.assertFalse;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.persistence.model.PricingPlan;
import com.example.persistence.repository.PricingPlanRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PricingPlanRepositoryTest {
	@Autowired
    private TestEntityManager entityManager;
	
	@Autowired
    private PricingPlanRepository pricingPlanRepository;
	
	@Test
	public void testGetOnlyActivePlans() {
	    // given
		PricingPlan plan = new PricingPlan();
		plan.setCountry("US");
		plan.setStartDate(new Date());
		plan.setPlanTypeId(1);
		plan.setPrice(new BigDecimal(10.00));
		plan.setDeleted(false);
		
		PricingPlan plan2 = new PricingPlan();
		plan2.setCountry("US");
		plan2.setStartDate(new Date());
		plan2.setPlanTypeId(1);
		plan2.setPrice(new BigDecimal(10.00));
		plan2.setDeleted(true);
		
		entityManager.persist(plan);
	    entityManager.persist(plan2);
	    entityManager.flush();
	 
	    List<PricingPlan> plans = pricingPlanRepository.findByCountryAndEndDateAndDeleted("US", null, false);
	    
	    for(PricingPlan pPlan : plans) {
	    	assertFalse(pPlan.isDeleted());
	    }
	}
	
}
