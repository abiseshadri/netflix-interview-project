package com.example.persistence.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.example.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table
public class PricingPlan {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long planId; 
	
	@Column(unique=false, nullable=false)
	private int planTypeId;
	
	@Column(unique=false, nullable=false, columnDefinition="VARCHAR(2)")
	private String country; 
	
	@Column(unique=false, nullable=false, columnDefinition="TIMESTAMP")
	@Temporal(value=TemporalType.TIMESTAMP)
	@JsonSerialize(using = JsonDateSerializer.class)
	private Date startDate;
	
	@Column(unique=false, nullable=true, columnDefinition="TIMESTAMP")
	@Temporal(value=TemporalType.TIMESTAMP)
	@JsonSerialize(using = JsonDateSerializer.class)
	private Date endDate;
	
	@Column(unique=false, nullable=false)
	private BigDecimal price;
	
	@Column(unique=false, nullable=true)
	@JsonIgnore
	private boolean deleted;

	public long getPlanId() {
		return planId;
	}

	public void setPlanId(long planId) {
		this.planId = planId;
	}

	public int getPlanTypeId() {
		return planTypeId;
	}

	public void setPlanTypeId(int planTypeId) {
		this.planTypeId = planTypeId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setStartDate(long startDate) {
		this.startDate = new Date(startDate * 1000);
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}