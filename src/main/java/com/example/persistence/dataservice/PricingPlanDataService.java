package com.example.persistence.dataservice;

import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.exception.PlanNotFoundException;
import com.example.persistence.model.PricingPlan;
import com.example.persistence.repository.PricingPlanRepository;
/**
 * Data service exposing access to the Pricing Plan table. 
 * Some methods are pass through of the s
 * @author abise
 *
 */
@Service
public class PricingPlanDataService {

	@Autowired
	PricingPlanRepository pricingPlanRepository;
	
	/**
	 * Find plan by id
	 * @param planId
	 * @return
	 * @throws PlanNotFoundException
	 */
	public PricingPlan findByPlanId(long planId) throws PlanNotFoundException {
		return pricingPlanRepository.findByPlanIdAndDeleted(planId, false)
				.orElseThrow(() -> 
				new PlanNotFoundException("No pricing plan found with id: " + planId));
	}
	
	/**
	 * Finds the plan by Id and applies end date
	 * @param planId
	 * @param endDate
	 * @return
	 * @throws PlanNotFoundException - if no plan found
	 */
	@Transactional
	public PricingPlan updatePricingPlanEndDate(long planId, Date endDate) throws PlanNotFoundException{
		PricingPlan plan = pricingPlanRepository.findById(planId)
		.orElseThrow(() -> 
		new PlanNotFoundException("No pricing plan found with id: " + planId));
		
		plan.setEndDate(endDate);
		return pricingPlanRepository.save(plan);
	}
	
	public List<PricingPlan> findActivePlansByCountry(String country){
		return pricingPlanRepository.findByCountryAndEndDateAndDeleted(country, null, false);
	}
	
	/**
	 * Finds any active plans matching that service plan type id and country and
	 * updates the end date. Then persists the new end date.
	 * 
	 * @param plan - plan details to save
	 * @param oldEffectiveEndDate - end date for already existing plans
	 * @return
	 */
	@Transactional
	public PricingPlan createNewPricingPlan(PricingPlan plan, Date oldEffectiveEndDate) {
		List<PricingPlan> matchingActivePlans = pricingPlanRepository
				.findByCountryAndPlanTypeIdAndEndDateAndDeleted(plan.getCountry(), plan.getPlanTypeId(), null, false);
		
		for(PricingPlan activePlan : matchingActivePlans) {
			activePlan.setEndDate(oldEffectiveEndDate);
		}
		if (!matchingActivePlans.isEmpty()) {
			pricingPlanRepository.saveAll(matchingActivePlans);
		}
		plan.setPlanId(0); //Prevents overwriting any existing plans
		return pricingPlanRepository.save(plan);
	}
	
	/**
	 * Deletes Plan By Id. Throws exception if plan not found
	 * @param planId
	 * @throws PlanNotFoundException 
	 */
	public void deletePricingPlan(long planId) throws PlanNotFoundException {
		PricingPlan plan = findByPlanId(planId);
		plan.setDeleted(true);
		pricingPlanRepository.save(plan);
	}

}