package com.example.persistence.repository;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.example.persistence.model.PricingPlan;

/**
 * Repository (DAO) for the PricingPlan Table. Used by data service for interacting with data
 * Implementation is auto generated on class load by Spring framework. See Spring-data-jpa
 * @author abise
 *
 */
public interface PricingPlanRepository extends CrudRepository<PricingPlan, Long> {

	Optional<PricingPlan> findByPlanIdAndDeleted(long planId, boolean deleted);
	
	List<PricingPlan> findByCountryAndEndDateAndDeleted(String country, Date endDate, boolean deleted);
	
	List<PricingPlan> findByCountryAndPlanTypeIdAndEndDateAndDeleted(String country, int planTypeId, Date endDate, boolean deleted);
}
