package com.example.dto;

import java.math.BigDecimal;
import java.util.Date;

public class PlanCreationDTO {

	private int planTypeId;
	
	private String country; 
	
	private Date startDate;
	
	private Date endDate;
	
	private BigDecimal price;
	
	private Date previousEffectiveEndDate;

	public Date getPreviousEffectiveEndDate() {
		return previousEffectiveEndDate;
	}

	public void setPreviousEffectiveEndDate(Date previousEffectiveEndDate) {
		this.previousEffectiveEndDate = previousEffectiveEndDate;
	}
	
	public void setPreviousEffectiveEndDate(long previousEffectiveEndDate) {
		this.previousEffectiveEndDate = new Date(previousEffectiveEndDate * 1000);
	}

	public int getPlanTypeId() {
		return planTypeId;
	}

	public void setPlanTypeId(int planTypeId) {
		this.planTypeId = planTypeId;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public void setStartDate(long startDate) {
		this.startDate = new Date(startDate * 1000);
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public void setEndDate(long endDate) {
		this.endDate = new Date(endDate * 1000);
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
