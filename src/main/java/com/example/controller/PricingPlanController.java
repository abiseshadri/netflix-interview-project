package com.example.controller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.PlanCreationDTO;
import com.example.exception.PlanNotFoundException;
import com.example.persistence.dataservice.PricingPlanDataService;
import com.example.persistence.model.PricingPlan;

/**
 * Main controller for update, retrieval of pricing plans
 * 
 * @author abise
 *
 */
@RestController
public class PricingPlanController {

	@Autowired
	PricingPlanDataService pricingPlanDataService;

	@RequestMapping(value = "/{country}/plans/", method = RequestMethod.GET)
	public @ResponseBody List<PricingPlan> findActivePlans(@PathVariable("country") String country) {
		return pricingPlanDataService.findActivePlansByCountry(country);
	}

	@RequestMapping(value = "/{country}/plans/{planId}", method = RequestMethod.GET)
	public @ResponseBody PricingPlan findPlanById(@PathVariable("country") String country,
			@PathVariable("planId") long planId) throws PlanNotFoundException {
		return pricingPlanDataService.findByPlanId(planId);
	}

	@RequestMapping(value = "/{country}/plans/{planId}", params = { "billingDate" }, method = RequestMethod.GET)
	public ResponseEntity<?> findPlanForBillingDate(@PathVariable("country") String country,
			@PathVariable("planId") long planId, @RequestParam("billingDate") long billingTimestamp)
			throws PlanNotFoundException {
		PricingPlan plan = pricingPlanDataService.findByPlanId(planId);
		Date billingDate = new Date(billingTimestamp * 1000);
		if (plan.getCountry() != null && !plan.getCountry().equalsIgnoreCase((country))) {
			throw new PlanNotFoundException("Plan doesn't match the country in request!");
		} else if (plan.getStartDate().compareTo(billingDate) > 0
				|| plan.getEndDate() != null && plan.getEndDate().compareTo(billingDate) < 0) {
			return new ResponseEntity<Object>("Invalid billing date for plan", new HttpHeaders(),
					HttpStatus.BAD_REQUEST);
		}
		BigDecimal price = plan.getPrice();
		return new ResponseEntity<BigDecimal>(price, HttpStatus.OK);
	}

	@RequestMapping(value = "/{country}/plans/{planId}", method = RequestMethod.PUT)
	public @ResponseBody PricingPlan updatePlanWithEndDate(@PathVariable("country") String country,
			@PathVariable("planId") long planId, @RequestBody long endDate) throws PlanNotFoundException {
		Date effectiveEndDate = new Date(endDate * 1000);
		return pricingPlanDataService.updatePricingPlanEndDate(planId, effectiveEndDate);
	}

	@RequestMapping(value = "/{country}/plans/", method = RequestMethod.POST, consumes = "application/json")
	public @ResponseBody PricingPlan addPlan(@RequestBody PlanCreationDTO planDTO) throws PlanNotFoundException {
		PricingPlan plan = new PricingPlan();
		plan.setCountry(planDTO.getCountry());
		plan.setPlanTypeId(planDTO.getPlanTypeId());
		plan.setStartDate(planDTO.getStartDate());
		plan.setPrice(planDTO.getPrice());

		// Default date parameter
		Date oldEffectiveEndDate = planDTO.getPreviousEffectiveEndDate();
		if (oldEffectiveEndDate == null) {
			oldEffectiveEndDate = new Date(plan.getStartDate().getTime() - 1000);
		}
		return pricingPlanDataService.createNewPricingPlan(plan, oldEffectiveEndDate);
	}

	@RequestMapping(value = "/{country}/plans/{planId}", method = RequestMethod.DELETE)
	public void deletePlanById(@PathVariable("country") String country, @PathVariable("planId") long planId)
			throws PlanNotFoundException {
		pricingPlanDataService.deletePricingPlan(planId);
	}
}
